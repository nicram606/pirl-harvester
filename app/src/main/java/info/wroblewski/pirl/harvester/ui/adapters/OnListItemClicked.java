package info.wroblewski.pirl.harvester.ui.adapters;

import java.io.File;

public interface OnListItemClicked {
    public void sessionSelected(File sessionFolder);
}
