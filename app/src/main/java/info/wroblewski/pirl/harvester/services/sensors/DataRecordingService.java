package info.wroblewski.pirl.harvester.services.sensors;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Sensor;
import android.os.IBinder;
import android.util.Log;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import info.wroblewski.pirl.harvester.services.sensors.data.SensorEventDetails;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DataRecordingService extends Service {

    SensorsDataGatherService dataGatherService;
    boolean isDataGatherServiceBounded = false;

    static final int MAX_ELEMENTS_IN_BUFFER = 250;
    String sessionFolderName,
            accelerometerFileName = "accelerometer.csv",
            gyroFileName = "gyro.csv";
    File sessionFolder;

    List<String[]>  accelerometerValues, gyroValues;
    CSVWriter accelerometerWriter, gyroWriter;

    public DataRecordingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sessionFolderName = createSessionFolderName();
        sessionFolder = createFolderForSession(sessionFolderName);
        setupFilesForSensors(sessionFolder);

        Intent dataGatherServiceIntent = new Intent(this, SensorsDataGatherService.class);
        bindService(dataGatherServiceIntent, dataGatherServiceConnection, Context.BIND_AUTO_CREATE);

        return START_STICKY;
    }

    private String createSessionFolderName() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        return formatter.format(new Date());
    }

    @Override
    public void onDestroy() {
        try {
            flushAndCloseFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private File createFolderForSession(String folderName) {
        File sessionFolder = new File(getApplicationContext().getFilesDir(), folderName);
        if (!sessionFolder.mkdir()) {
            Log.e(getClass().getName(), "Cannot create session folder!");
        } else {
            Log.v(getClass().getName(), "Session folder created");
        }

        return sessionFolder;
    }

    private void setupFilesForSensors(File sessionFolder) {
        try {
            File accelerometerFile = new File(sessionFolder, accelerometerFileName);
            accelerometerFile.createNewFile();
            accelerometerWriter = new CSVWriter(new FileWriter(accelerometerFile));
            File gyroFile = new File(sessionFolder, gyroFileName);
            gyroFile.createNewFile();
            gyroWriter = new CSVWriter(new FileWriter(gyroFile));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void showCancelRecordingNotification() {
        // TODO
    }

    void flushAndCloseFiles() throws IOException {
        accelerometerWriter.writeAll(accelerometerValues);
        gyroWriter.writeAll(gyroValues);

        accelerometerWriter.flush();
        accelerometerWriter.close();
        gyroWriter.flush();
        gyroWriter.close();
    }

    private ServiceConnection dataGatherServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            SensorsDataGatherService.Binder binder = (SensorsDataGatherService.Binder) iBinder;
            dataGatherService = binder.getService();
            isDataGatherServiceBounded = true;

            dataGatherService
                    .getSensorEventObservable()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Observer<SensorEventDetails>() {
                        boolean firstAccelerometerValues, firstGyroValues;

                        @Override
                        public void onSubscribe(Disposable d) {
                            accelerometerValues = new ArrayList<>();
                            gyroValues = new ArrayList<>();
                            firstAccelerometerValues = firstGyroValues = true;
                        }

                        @Override
                        public void onNext(SensorEventDetails details) {
                            switch (details.getSensorCode()) {
                                case Sensor.TYPE_ACCELEROMETER:
                                    if (firstAccelerometerValues) {
                                        accelerometerValues.add(details.createCSVStringsFromSensorIds());
                                        firstAccelerometerValues = false;
                                    }

                                    accelerometerValues.add(details.createCSVStringFromValues());

                                    if (accelerometerValues.size() > MAX_ELEMENTS_IN_BUFFER) {
                                        accelerometerWriter.writeAll(accelerometerValues);
                                        try {
                                            accelerometerWriter.flush();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        accelerometerValues = new ArrayList<>();
                                    }
                                    break;

                                case Sensor.TYPE_GYROSCOPE:
                                    if (firstGyroValues) {
                                        gyroValues.add(details.createCSVStringsFromSensorIds());
                                        firstGyroValues = false;
                                    }

                                    gyroValues.add(details.createCSVStringFromValues());

                                    if (gyroValues.size() > MAX_ELEMENTS_IN_BUFFER) {
                                        gyroWriter.writeAll(gyroValues);
                                        try {
                                            gyroWriter.flush();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        gyroValues = new ArrayList<>();
                                    }
                                    break;

                                default:
                                    Log.e("Sensor service value", "Received SensorEventDetails object for not handled sensor type id: " + details.getSensorCode());
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isDataGatherServiceBounded = false;

            try {
                flushAndCloseFiles();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };
}
