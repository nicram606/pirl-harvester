package info.wroblewski.pirl.harvester.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import info.wroblewski.pirl.harvester.R;

public class SessionsListAdapter extends BaseAdapter {

    private List<OnListItemClicked> listeners = new ArrayList<>();
    private Context context;
    private File[] data;

    public SessionsListAdapter(Context context, File[] data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public File getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LinearLayout root;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            root = (LinearLayout) inflater.inflate(R.layout.session_adapter_element, null);
        } else {
            root = (LinearLayout) convertView;
        }

        ((TextView) (root.findViewById(R.id.text))).setText(data[position].getName());

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyWatchers(position);
            }
        });

        return root;
    }

    public void addListener(OnListItemClicked listener) {
        listeners.add(listener);
    }

    private void notifyWatchers(int positionClicked) {
        for (OnListItemClicked listener : listeners) {
            listener.sessionSelected(data[positionClicked]);
        }
    }
}