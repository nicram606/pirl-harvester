package info.wroblewski.pirl.harvester.services.sensors.data;

public class SensorValue {

    private int accuracy;
    private double value;
    private long timestamp;

    public SensorValue(int accuracy, double value, long timestamp) {
        this.accuracy = accuracy;
        this.value = value;
        this.timestamp = timestamp;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
