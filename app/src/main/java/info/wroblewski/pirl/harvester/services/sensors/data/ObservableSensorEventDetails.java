package info.wroblewski.pirl.harvester.services.sensors.data;

import java.util.ArrayList;
import java.util.List;

import info.wroblewski.pirl.harvester.services.sensors.data.interfaces.SensorEventDetailsChangedListener;

public class ObservableSensorEventDetails {

    private SensorEventDetails sensorEventDetails;
    private List<SensorEventDetailsChangedListener> listeners = new ArrayList<>();

    public ObservableSensorEventDetails(SensorEventDetails sensorEventDetails) {
        this.sensorEventDetails = sensorEventDetails;
    }

    public ObservableSensorEventDetails() {}

    public SensorEventDetails getSensorEventDetails() {
        return sensorEventDetails;
    }

    public void setSensorEventDetails(SensorEventDetails sensorEventDetails) {
        this.sensorEventDetails = sensorEventDetails;
        notifyListeners();
    }

    void notifyListeners() {
        for (SensorEventDetailsChangedListener listener : listeners) {
            listener.onChanged(this.sensorEventDetails);
        }
    }

    public void addListener(SensorEventDetailsChangedListener listener) {
        this.listeners.add(listener);
    }
}
