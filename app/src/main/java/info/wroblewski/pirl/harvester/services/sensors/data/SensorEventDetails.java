package info.wroblewski.pirl.harvester.services.sensors.data;

import java.util.List;

public class SensorEventDetails {

    private int sensorCode, sensorVersion;
    private String sensorVendor;
    private List<SensorValue> sensorValues;

    public SensorEventDetails(int sensorCode, String sensorVendor, int sensorVersion, List<SensorValue> sensorValues) {
        this.sensorCode = sensorCode;
        this.sensorVendor = sensorVendor;
        this.sensorVersion = sensorVersion;
        this.sensorValues = sensorValues;
    }

    public int getSensorCode() {
        return sensorCode;
    }

    public void setSensorCode(int sensorCode) {
        this.sensorCode = sensorCode;
    }

    public String getSensorVendor() {
        return sensorVendor;
    }

    public void setSensorVendor(String sensorVendor) {
        this.sensorVendor = sensorVendor;
    }

    public int getSensorVersion() {
        return sensorVersion;
    }

    public void setSensorVersion(int sensorVersion) {
        this.sensorVersion = sensorVersion;
    }

    public List<SensorValue> getSensorValues() {
        return sensorValues;
    }

    public void setSensorValues(List<SensorValue> sensorValues) {
        this.sensorValues = sensorValues;
    }

    public String[] createCSVStringsFromSensorIds() {
        return new String[] {
                "Sensor code", String.valueOf(getSensorCode()),
                "Sensor vendor", getSensorVendor(),
                "Sensor version", String.valueOf(getSensorVersion())
        };
    }

    public String[] createCSVStringFromValues() {
        String values[] = new String[5];

        values[0] = String.valueOf(System.currentTimeMillis());
        for (int i = 0; i < sensorValues.size(); i++) {
            values[i + 1] = String.valueOf(sensorValues.get(i).getValue());
        }
        values[4] = String.valueOf(sensorValues.get(0).getAccuracy());

        for (int i = 0; i < values.length; i++) {
            values[i] = values[i].replace("\"", "").replace(".", ",");
        }
        return values;
    }
}
