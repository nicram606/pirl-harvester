package info.wroblewski.pirl.harvester.utils;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import lecho.lib.hellocharts.model.PointValue;

public class ParcelablePointValue extends PointValue implements Parcelable {
    private PointValue pointValue;

    public ParcelablePointValue(PointValue pointValue) {
        this.pointValue = pointValue;
    }

    public PointValue getPointValue() {
        return pointValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.pointValue.getX());
        dest.writeFloat(this.pointValue.getY());
        dest.writeString(new String(this.pointValue.getLabelAsChars()));
    }

    protected ParcelablePointValue(Parcel in) {
        PointValue pointValue = new PointValue();
        pointValue.set(in.readFloat(), in.readFloat());

        String label = in.readString();
        if (label != null) {
            pointValue.setLabel(label);
        }
        this.pointValue = pointValue;
    }

    public static final Parcelable.Creator<ParcelablePointValue> CREATOR = new Parcelable.Creator<ParcelablePointValue>() {
        @Override
        public ParcelablePointValue createFromParcel(Parcel source) {
            return new ParcelablePointValue(source);
        }

        @Override
        public ParcelablePointValue[] newArray(int size) {
            return new ParcelablePointValue[size];
        }
    };
}
