package info.wroblewski.pirl.harvester.services.sensors;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.SparseIntArray;

import java.util.ArrayList;
import java.util.List;

import info.wroblewski.pirl.harvester.services.sensors.data.ObservableSensorEventDetails;
import info.wroblewski.pirl.harvester.services.sensors.data.SensorEventDetails;
import info.wroblewski.pirl.harvester.services.sensors.data.SensorValue;
import info.wroblewski.pirl.harvester.services.sensors.data.interfaces.SensorEventDetailsChangedListener;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class SensorsDataGatherService extends Service {
    private SensorManager sensorsManager;
    private List<Sensor> sensors;
    private SensorEventListener sensorEventListener;
    private Observable<SensorEventDetails> sensorEventObservable;
    private ObservableSensorEventDetails observableSensorEventDetails
            = new ObservableSensorEventDetails();
    private IBinder binder = new Binder();

    public SensorsDataGatherService() {}

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.sensorsManager = prepareSensorManager(getApplicationContext());
        this.sensors = getAvailableSensors(sensorsManager);
        sensorEventObservable = createSensorEventObservable(observableSensorEventDetails);
        sensorEventListener = prepareSensorEventListener(observableSensorEventDetails);
        new Thread(new Runnable() {
            @Override
            public void run() {
                registerSensors(sensors, sensorsManager, sensorEventListener);
            }
        }).run();
        return START_STICKY;
    }

    private Observable<SensorEventDetails> createSensorEventObservable(
            final ObservableSensorEventDetails sensorEventDetails) {
        return Observable.create(new ObservableOnSubscribe<SensorEventDetails>() {
            @Override
            public void subscribe(final ObservableEmitter<SensorEventDetails> emitter) {
                sensorEventDetails.addListener(new SensorEventDetailsChangedListener() {
                    @Override
                    public void onChanged(SensorEventDetails sensorEventDetails) {
                        emitter.onNext(sensorEventDetails);
                    }
                });
            }
        });
    }

    private SensorEventListener prepareSensorEventListener(final ObservableSensorEventDetails
                                                                   sensorEventDetails) {
        return new SensorEventListener() {
            SparseIntArray sensorCurrentAccuracy = new SparseIntArray();
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                List<SensorValue> sensorValues = new ArrayList<>();

                for (int i = 0; i < sensorEvent.values.length; i++) {
                    sensorValues.add(new SensorValue(
                            sensorCurrentAccuracy.get(sensorEvent.sensor.getType(),
                                    SensorManager.SENSOR_STATUS_UNRELIABLE),
                            sensorEvent.values[i],
                            sensorEvent.timestamp
                    ));
                }

                sensorEventDetails.setSensorEventDetails(new SensorEventDetails(
                        sensorEvent.sensor.getType(),
                        sensorEvent.sensor.getVendor(),
                        sensorEvent.sensor.getVersion(),
                        sensorValues
                ));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                sensorCurrentAccuracy.put(sensor.getType(), accuracy);
            }
        };
    }

    private SensorManager prepareSensorManager(Context context) {
        return (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    private List<Sensor> getAvailableSensors(SensorManager sensorManager) {
        List<Sensor> sensors = new ArrayList<>();
        // TODO exclude list below to other file
        int sensorsInQuestion[] = {
                Sensor.TYPE_ACCELEROMETER,
                Sensor.TYPE_GYROSCOPE
        };

        for (int sensorCode : sensorsInQuestion) {
            if (sensorManager.getDefaultSensor(sensorCode) != null)
                sensors.add(sensorManager.getDefaultSensor(sensorCode));
        }

        return sensors;
    }

    private void registerSensors(
                                List<Sensor> sensorsToRegister,
                                SensorManager sensorManager,
                                SensorEventListener listener) {
        for (Sensor sensor : sensorsToRegister) {
            sensorManager.registerListener(
                    listener,
                    sensor,
                    SensorManager.SENSOR_DELAY_FASTEST
            );
        }
    }

    private void unregisterSensors(List<Sensor> sensorsToRegister,
                                   SensorManager sensorManager,
                                   SensorEventListener listener) {
        if (sensorsToRegister == null) return;

        for (Sensor sensor : sensorsToRegister) {
            sensorManager.unregisterListener(
                    listener, sensor
            );
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterSensors(sensors, sensorsManager, sensorEventListener);
    }

    public Observable<SensorEventDetails> getSensorEventObservable() {
        return sensorEventObservable;
    }

    public class Binder extends android.os.Binder {
        public SensorsDataGatherService getService() {
            return SensorsDataGatherService.this;
        }
    }
}
