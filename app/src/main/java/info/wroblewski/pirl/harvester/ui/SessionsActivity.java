package info.wroblewski.pirl.harvester.ui;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.File;

import info.wroblewski.pirl.harvester.R;
import info.wroblewski.pirl.harvester.ui.adapters.OnListItemClicked;
import info.wroblewski.pirl.harvester.ui.adapters.SessionsListAdapter;

public class SessionsActivity extends AppCompatActivity {

    File rootFolder;
    File[] filesList;
    ListView sessionsListView;
    File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
            "Harvester sessions");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);

        rootFolder = getApplicationContext().getFilesDir();
        filesList = rootFolder.listFiles();
        sessionsListView = (ListView) findViewById(R.id.sessions_list_view);
        SessionsListAdapter adapter = new SessionsListAdapter(getApplicationContext(), filesList);

        adapter.addListener(new OnListItemClicked() {
            @Override
            public void sessionSelected(final File sessionFolder) {
                Toast.makeText(getApplicationContext(), "Zipping session files into SD Card...", Toast.LENGTH_LONG).show();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.w("Zipper", "Session zip save into " + zipper(sessionFolder, sessionFolder.getName()));
                    }
                }).run();

            }
        });
        sessionsListView.setAdapter(adapter);
    }

    public String zipper(File folder, String zipFileName) {
        mediaStorageDir.mkdirs();
        String zippath = mediaStorageDir.getAbsolutePath() + "/" + zipFileName +  ".zip";
        try {
            if (new File(zippath).exists()) {
                new File(zippath).delete();
            }
            ZipFile zipFile = new ZipFile(zippath);
            ZipParameters zipParameters = new ZipParameters();
            zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
            zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);

            zipFile.addFolder(folder, zipParameters);
        }
        catch (ZipException e) {
            e.printStackTrace();
        }
        return zippath;
    }
}
