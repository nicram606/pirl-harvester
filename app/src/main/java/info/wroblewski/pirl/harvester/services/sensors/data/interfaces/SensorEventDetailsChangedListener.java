package info.wroblewski.pirl.harvester.services.sensors.data.interfaces;

import info.wroblewski.pirl.harvester.services.sensors.data.SensorEventDetails;

public interface SensorEventDetailsChangedListener {
    public void onChanged(SensorEventDetails sensorEventDetails);
}
