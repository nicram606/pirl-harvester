package info.wroblewski.pirl.harvester.ui;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.PermissionCallback;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import info.wroblewski.pirl.harvester.R;
import info.wroblewski.pirl.harvester.services.sensors.DataRecordingService;
import info.wroblewski.pirl.harvester.services.sensors.SensorsDataGatherService;
import info.wroblewski.pirl.harvester.services.sensors.data.SensorEventDetails;
import info.wroblewski.pirl.harvester.services.sensors.data.SensorValue;
import info.wroblewski.pirl.harvester.utils.ParcelablePointValue;
import info.wroblewski.pirl.harvester.utils.ServiceDetails;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lecho.lib.hellocharts.gesture.ContainerScrollType;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

public class MainActivity extends AppCompatActivity {

    final int MAX_X_VALUES_ON_AXIS = 50;
    final int CHART_UPDATE_FREQUENCY = 10;
    boolean areReadWritePermissionsAreGranted = false;

    SensorsDataGatherService dataGatherService;
    boolean isDataGatherServiceBounded = false;
    LineChartView accelerometerChart, gyroChart;
    List<List<PointValue>> accelerometerChartValues = new ArrayList<>(),
            gyroChartValues = new ArrayList<>();

    int[] colors = {
            Color.RED,
            Color.GREEN,
            Color.BLUE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        accelerometerChart = findViewById(R.id.accelerometer_chart);
        gyroChart = findViewById(R.id.gyroscope_chart);
        for (int i = 0; i < 3; i++) {
            accelerometerChartValues.add(new Stack<PointValue>());
            gyroChartValues.add(new Stack<PointValue>());
        }

        areReadWritePermissionsAreGranted = checkReadWritePermissions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.charts_activity_menu, menu);

        menu.findItem(R.id.sensors_service_toggle).setChecked(
                ServiceDetails.isRunning(getApplicationContext(), SensorsDataGatherService.class)
        );
        menu.findItem(R.id.file_save_service_toggle).setChecked(
                ServiceDetails.isRunning(getApplicationContext(), DataRecordingService.class)
        );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.sensors_service_toggle:
                if (!item.isChecked()) {
                    startAndBindDataGatherService();
                } else {
                    Intent intent = new Intent(this, SensorsDataGatherService.class);
                    unbindService(dataGatherServiceConnection);
                    isDataGatherServiceBounded = false;
                    stopService(intent);
                }

                item.setChecked(!item.isChecked());
                return false;
            case R.id.file_save_service_toggle:
                if (!item.isChecked()) {
                    if (!areReadWritePermissionsAreGranted) {
                        new AskPermission.Builder(this)
                                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                .setCallback(new PermissionCallback() {
                                    @Override
                                    public void onPermissionsGranted(int requestCode) {
                                        Log.d("Permissions granted", "" + requestCode);
                                        areReadWritePermissionsAreGranted = checkReadWritePermissions();
                                    }

                                    @Override
                                    public void onPermissionsDenied(int requestCode) {
                                        Log.d("Permissions denied", "" + requestCode);
                                        areReadWritePermissionsAreGranted = checkReadWritePermissions();
                                    }
                                })
                                .request(1);
                        return true;
                    }

                    Intent dataRecordingService = new Intent(this, DataRecordingService.class);
                    startService(dataRecordingService);
                } else {
                    Intent dataRecordingService = new Intent(this, DataRecordingService.class);
                    stopService(dataRecordingService);
                }

                item.setChecked(!item.isChecked());
                return false;
            case R.id.list_sessions:
                startActivity(new Intent(this, SessionsActivity.class));
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void updateChart(List<List<PointValue>> chartValues, LineChartView chartView) {
        int i = 0;
        List<Line> lines = new ArrayList<>();
        for (List<PointValue> values : chartValues) {
            Line line = new Line(values)
                    .setColor(colors[i])
                    .setCubic(true);

            lines.add(line);

            i++;
            i %= chartValues.size();
        }

        LineChartData data = new LineChartData();
        data.setAxisYLeft(new Axis());
        data.setLines(lines);

        chartView.setLineChartData(data);
    }

    private boolean checkReadWritePermissions() {
        String permissions[] = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        for (String permission : permissions) {
            int res = getApplicationContext().checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    void startAndBindDataGatherService() {
        startDataGatherService();
        bindDataGatherService();
    }

    void startDataGatherService() {
        Intent intent = new Intent(this, SensorsDataGatherService.class);
        startService(intent);
    }

    void bindDataGatherService() {
        Intent intent = new Intent(this, SensorsDataGatherService.class);
        bindService(intent, dataGatherServiceConnection, Context.BIND_AUTO_CREATE);
    }


    @Override
    protected void onResume() {
        super.onResume();

        boolean isSensorsDataGatherServiceRunning = ServiceDetails.isRunning(this, SensorsDataGatherService.class);
        if (!isDataGatherServiceBounded && isSensorsDataGatherServiceRunning) {
            bindDataGatherService();
        } else if (!isSensorsDataGatherServiceRunning) {
            startAndBindDataGatherService();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isDataGatherServiceBounded) {
            unbindService(dataGatherServiceConnection);
            isDataGatherServiceBounded = false;
            if (serviceObserver != null) {
                serviceObserver.dispose();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (isDataGatherServiceBounded) {
            unbindService(dataGatherServiceConnection);
        }

    }

    Disposable serviceObserver;
    private ServiceConnection dataGatherServiceConnection = new ServiceConnection() {
        Observable<SensorEventDetails> eventDetailsObservable;
        Observer<SensorEventDetails> observer = new Observer<SensorEventDetails>() {
            List<List<PointValue>> accelerometerValues, gyroValues;
            int accelerometerSuspender = 0, gyroscopeSuspender = 0;

            @Override
            public void onSubscribe(Disposable d) {
                accelerometerValues = (accelerometerChartValues.isEmpty()) ?
                        prepareListOfLists() : accelerometerChartValues;
                gyroValues = (gyroChartValues.isEmpty()) ?
                        prepareListOfLists() : gyroChartValues;

                serviceObserver = d;
            }

            @Override
            public void onNext(SensorEventDetails details) {
                switch (details.getSensorCode()) {
                    case Sensor.TYPE_ACCELEROMETER:
                        if (accelerometerSuspender > CHART_UPDATE_FREQUENCY) {
                            handleAddingValues(accelerometerValues,
                                    details.getSensorValues(),
                                    accelerometerChart);
                            accelerometerSuspender = 0;
                        }
                        accelerometerSuspender++;
                        break;

                    case Sensor.TYPE_GYROSCOPE:
                        if (gyroscopeSuspender > CHART_UPDATE_FREQUENCY) {
                            handleAddingValues(gyroValues,
                                    details.getSensorValues(),
                                    gyroChart);
                            gyroscopeSuspender = 0;
                        }
                        gyroscopeSuspender++;
                        break;

                    default:
                        Log.e("Sensor service value", "Received SensorEventDetails object for not handled sensor type id: " + details.getSensorCode());
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }

            private List<List<PointValue>> prepareListOfLists() {
                List<List<PointValue>> lists = new ArrayList<>();
                lists.add(new ArrayList<PointValue>());
                lists.add(new ArrayList<PointValue>());
                lists.add(new ArrayList<PointValue>());

                return lists;
            }

            private PointValue createPointValueFromSensorValue(SensorValue sensorValue) {
                return new PointValue(
                        sensorValue.getTimestamp(),
                        (float) sensorValue.getValue());
            }

            private List<List<PointValue>> reduceValues(List<List<PointValue>> values, int limit, int by) {
                for (int i = 0; i < values.size(); i++) {
                    List<PointValue> currentList = values.get(i);
                    currentList.remove(0);
                    values.set(i, currentList);
                }

                return values;
            }

            private List<List<PointValue>> handleAddingValues(
                    List<List<PointValue>> values,
                    List<SensorValue> sensorValues,
                    LineChartView chartViewToUpdate) {
                int i = 0;
                for (SensorValue sensorValue : sensorValues) {
                    values.get(i)
                            .add(createPointValueFromSensorValue(sensorValue));
                    i++;
                }

                int valuesListSize = values.get(0).size();
                updateChart(values, chartViewToUpdate);
                if (valuesListSize >= MAX_X_VALUES_ON_AXIS) {
                    values = reduceValues(
                            values,
                            MAX_X_VALUES_ON_AXIS,
                            CHART_UPDATE_FREQUENCY);
                }

                return values;
            }
        };

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            SensorsDataGatherService.Binder binder = (SensorsDataGatherService.Binder) iBinder;
            dataGatherService = binder.getService();
            isDataGatherServiceBounded = true;

            eventDetailsObservable = dataGatherService
                    .getSensorEventObservable();

            eventDetailsObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(observer);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isDataGatherServiceBounded = false;
        }
    };
}
